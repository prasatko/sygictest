package sygictest.android.partnermb.cz.sygictest;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * User: honzik
 * Date: 30.9.13
 * Time: 21:24
 */
public class AndroidLogger implements Logger {

    public static final String TAG = "hellod";
    public static AndroidLogger _instance;
    private final ArrayList<String> logItems;
    private final ArrayAdapter<String> logAdapter;
    private File logFile;
    private SimpleDateFormat simpleDateFormat;
    private Application application;
    private org.apache.log4j.Logger fileLogger;
    private File logDir;


    public void setLogDir(File logDir) {
        this.logDir = logDir;
        logFile = new File(logDir, "log.txt");
        LogConfigurator logConfig = new LogConfigurator();
        logConfig.setFileName(logFile.getAbsolutePath());
        logConfig.setMaxFileSize(1024*1024*8);
        logConfig.setMaxBackupSize(10);
        logConfig.setRootLevel(org.apache.log4j.Level.DEBUG);
        logConfig.setLevel("org.apache", org.apache.log4j.Level.ERROR);
        logConfig.setUseFileAppender(true);
        logConfig.setUseLogCatAppender(false);
        logConfig.setImmediateFlush(true);
        logConfig.setFilePattern("%d - [%p]: %m%n");
        logConfig.configure();
        fileLogger = org.apache.log4j.Logger.getLogger("hellod");
    }

    public AndroidLogger(Application application) {
        this.application = application;
        logItems = new ArrayList<String>();
        logAdapter = new ArrayAdapter<String>(application, android.R.layout.simple_list_item_1, logItems);
        simpleDateFormat = new SimpleDateFormat("HH:mm");
        _instance = this;
    }

    @Override
    public void log(Level priority, String msg, Throwable t) {

        if(t != null) {
            msg += "\n" + Log.getStackTraceString(t);
        }
        Log.println(priority.intValue(), TAG, msg);

        final String message = msg;

        Looper looper = Looper.getMainLooper();
        Handler handler = new Handler(looper);
        handler.post(new Runnable() {
            @Override
            public void run() {
                logItems.add(message);
                if(logItems.size() > 100) {
                    logItems.remove(0);
                }
                logAdapter.notifyDataSetInvalidated();
            }
        });

        fileLogger.info(message);

    }

    @Override
    public void d(String message) {
        log(Level.DEBUG, message, null);
    }
    @Override
    public void i(String message) {
        log(Level.INFO, message, null);
    }
    @Override
    public void e(String message) {
        log(Level.ERROR, message, null);
    }
    @Override
    public void e(String message, Throwable t) {
        log(Level.ERROR, message, t);
    }

    public ListAdapter getListAdapter() {
        return logAdapter;
    }


}
