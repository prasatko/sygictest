package sygictest.android.partnermb.cz.sygictest;

import android.app.Application;
import android.os.Environment;

import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by honzik on 7.2.2017.
 */

public class TerminalApplication extends Application {

    NavigationManager navigationManager;
    AndroidLogger logger;

    @Override
    public void onCreate() {
        super.onCreate();
        logger = new AndroidLogger(this);
        File dataDir = new File(Environment.getExternalStorageDirectory(), "SygicTest");
        if(!dataDir.exists() ) {
            if(!dataDir.mkdirs()) {
                logger.e("attachments directory " + dataDir.getAbsolutePath() + " was not created");
            };
        }
        dataDir.setReadable(true);
        logger.setLogDir(dataDir);
        navigationManager = new NavigationManager(this, logger);
        navigationManager.init();

    }

}
