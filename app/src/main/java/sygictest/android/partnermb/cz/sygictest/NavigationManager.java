package sygictest.android.partnermb.cz.sygictest;

import android.os.Handler;
import android.os.RemoteException;

import com.sygic.sdk.remoteapi.Api;
import com.sygic.sdk.remoteapi.ApiCallback;
import com.sygic.sdk.remoteapi.ApiItinerary;
import com.sygic.sdk.remoteapi.ApiNavigation;
import com.sygic.sdk.remoteapi.events.ApiEvents;
import com.sygic.sdk.remoteapi.exception.GeneralException;
import com.sygic.sdk.remoteapi.model.StopOffPoint;

import java.util.ArrayList;



public class NavigationManager {
    private static final int MAX_TIME = 1000;
    private static final String PACKAGENAME = "com.sygic.fleet";
    private final TerminalApplication terminalApplication;
    private final Logger logger;
    private Api sygicApi;
    private ApiCallback apiCallback;
    private Handler handler = new Handler();
    private boolean connected;

    private SygicCommand sygicCommand;


    abstract class SygicCommand {
        abstract void run() throws Exception;

    };

    public NavigationManager(TerminalApplication terminalApplication, final Logger logger) {
        this.terminalApplication = terminalApplication;
        this.logger = logger;
        apiCallback = new ApiCallback() {

            @Override
            public void onEvent(final int i, final String s) {
                logger.i("navman: sygic.onEvent(" + i + ", data=" + s + ")");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        NavigationManager.this.onEvent(i, s);
                    }
                });

            }

            @Override
            public void onServiceConnected() {
                logger.i("navman: sygic.onServiceConnected");
                try {
                    logger.i("navman: calling sygicApi.registerCallback()");
                    sygicApi.registerCallback();
                    connected = true;
                } catch (Exception e) {
                    logger.e(e.getMessage(), e);
                }
            }

            @Override
            public void onServiceDisconnected() {
                logger.i("navman: sygic.onServiceDisconnected");
                try {
                    logger.i("navman: calling sygicApi.unregisterCallback()");
                    sygicApi.unregisterCallback();
                } catch (Exception e) {
                    logger.e("navman: error while sygicApi.unregisterCallback(): " + e.getMessage());
                }
                connected = false;
                logger.i("navman: calling sygicApi.connect() for reconnection");
                sygicApi.connect();
            }
        };
    }

    // runs sygic command - if sygic is not running we start it and delay the command until sygic starts
    private void runSygicCommand(SygicCommand command) {
        try {
            if(sygicApi.isAppRunning()) {
                logger.i("navman: runSygicCommand was called sygicApi.isAppRunning returned true - running command");
                command.run();
                logger.i("navman: runSygicCommand success end");
                return;
            } else {
                logger.i("navman: runSygicCommand was called but sygicApi.isAppRunning returned false - will call sygicApi.show(true) and rerun the command after sygic sends 1010 app_started event.");
                logger.i("navman: calling sygicApi.show(true)");
                sygicApi.show(true);
                logger.i("navman: calling sygicApi.show(true) end");
            }
        } catch (Exception e) {
            logger.e("navman: error while running sygic command, will try to connect and wait for sygic to be started");
        }

        this.sygicCommand = command;
    }

    /**
     * to be called when hellod starts
     */
    public void init() {
        logger.i("navman: NavigationManager.init()");
        try {
            logger.i("navman: calling sygicApi = Api.init(...)");
            sygicApi = Api.init(terminalApplication, PACKAGENAME, "com.sygic.fleet.SygicService", apiCallback);
            logger.i("navman: calling sygicApi.connect()");
            sygicApi.connect();

        } catch (Exception e) {
            logger.e("navman: error while NavigationManager.init()", e);
        }
    }


    public void close() {
        if(sygicApi != null) {
            logger.i("navman: calling sygicApi.disconnect()");
            sygicApi.disconnect();
        }
    }

    /**
     * brings sygic to foreground or starts it up if it is not running
     */
    public void showNavigation() {
        try {
            logger.i("navman: calling sygicApi.show(false)");
            sygicApi.show(false);
            logger.i("navman: calling sygicApi.show(false) end");
        } catch (Exception e) {
            logger.e(e.getMessage(), e);
        }
    }

    public boolean isSygicInForeground() {
        try {
            return Api.isApplicationInForeground(MAX_TIME);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * starts navigation to first working waypoint.
     */
    public void navigate(final ArrayList<StopOffPoint> points, final boolean forceShowNavigation) {
        logger.i("navman: NavigationManager.navigate, points: " + points.size());
        final String strItineraryName = "SygicTest";

        runSygicCommand(new SygicCommand() {
            @Override
            void run() throws Exception {
                if(forceShowNavigation) {
                    showNavigation();
                }
                try {
                    logger.i("navman: calling ApiItinerary.deleteItinerary()");
                    ApiItinerary.deleteItinerary(strItineraryName, MAX_TIME);
                    logger.i("navman: calling ApiItinerary.deleteItinerary() end");
                } catch (GeneralException e) {
                    logger.i("navman: error while deleting itinerary: " + e.getMessage());
                }
                logger.i("navman: calling ApiItinerary.addItinerary()");
                ApiItinerary.addItinerary(points, strItineraryName , MAX_TIME);
                logger.i("navman: calling ApiItinerary.addItinerary() end");
                logger.i("navman: calling ApiItinerary.setRoute");
                ApiItinerary.setRoute(strItineraryName, 0, MAX_TIME);
                logger.i("navman: calling ApiItinerary.setRoute end");
            }
        });
    }


    private void onEvent(int i, String s) {
        switch(i) {
            case ApiEvents.EVENT_WAIPOINT_VISITED:
                logger.i("navman: EVENT_WAIPOINT_VISITED(" + i + "," + s + ")");
                break;
            case ApiEvents.EVENT_ROUTE_FINISH:
                break;
            case ApiEvents.EVENT_APP_STARTED:
                handleSygicStarted();
                break;
            case ApiEvents.EVENT_APP_EXIT:
                handleSygicStop();
                break;
            case ApiEvents.EVENT_OFF_ROUTE:
                handleOffRoute();
        }
    }

    private void handleOffRoute() {
        logger.i("navman: handleOffRoute");
    }

    private void handleSygicStop() {
        logger.i("navman: sygicStop, doing nothing, sygic should be started with next sygic command");
    }

    private void handleSygicStarted() {
        logger.i("navman: sygicStarted");
        if(sygicCommand != null) {
            try {
                logger.i("navman: running sygic command after app_started event");
                sygicCommand.run();
                logger.i("navman: running sygic command after app_started event - success end");
            } catch (Exception e) {
                logger.e("navman: error while running sygic command after app_started event", e);
            } finally {
                sygicCommand = null;
            }
        }
    }

    public boolean isAppRunning() {
        logger.i("navman: calling sygicApi.isAppRunning()");
        try {
            boolean isRunning = sygicApi.isAppRunning();
            logger.i("navman: calling sygicApi.isAppRunning() end, returning " + isRunning);
            return isRunning;
        } catch (RemoteException e) {
            logger.i("navman: error while sygicApi.isAppRunning: " + e.getMessage());
        }
        return false;

    }



    public void getNavigationInfo2() {
        if(!isAppRunning()) return;
        try {
            if(!matchRoute()) {
                logger.d("navman: getNavigationInfo2: routeMatch.isRouteNavigating returned false, returning null");
                return;
            }
            logger.i("navman: calling ApiNavigation.getRouteStatus()");
            ApiNavigation.getRouteStatus(MAX_TIME);
        } catch (GeneralException e) {
            logger.e("navman: error while getting route status", e);
        }


    }

    public boolean matchRoute() {
        if(!isAppRunning()) {
            logger.d("navman: matchRoute: sygic not running, returning false");
            return false;
        }
        try {
            logger.d("navman: matchRoute: calling ApiItinerary.getItineraryList thread: " + Thread.currentThread().getName());
            ArrayList<StopOffPoint> itineraryList = ApiItinerary.getItineraryList("default", MAX_TIME);
            return true;
        } catch (GeneralException e) {
            logger.e("navman: error while getting route status", e);
        }
        return false;

    }

    void onActiveRouteChanged(ArrayList<StopOffPoint> newList) {
        boolean routeMatch = matchRoute();
        if(!routeMatch) {
            logger.i("navman: onActiveRouteChanged: sygic does not nagigate to current waypoint, will not reinitialize navigation");
            return;
        }
        logger.i("navman: onActiveRouteChanged: sygic nagigates to current waypoint, reinitializing navigation");
        navigate(newList, false);

    }


}
