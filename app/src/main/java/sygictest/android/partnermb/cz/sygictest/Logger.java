package sygictest.android.partnermb.cz.sygictest;

/**
 * User: honzik
 * Date: 16.11.13
 * Time: 8:32
 */
public interface Logger {
    enum Level {
        DEBUG(3), INFO(4), ERROR(6);
        private Level(int value) {this.value = value;}
        public int intValue() {return value;}
        private int value;
        };

    void log(Level priority, String msg, Throwable t);

    void d(String message);

    void i(String message);

    void e(String message);

    void e(String message, Throwable t);
}
