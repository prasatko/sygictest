package sygictest.android.partnermb.cz.sygictest;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.sygic.sdk.remoteapi.model.StopOffPoint;

import java.util.ArrayList;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final TerminalApplication application = (TerminalApplication) getApplication();

        ListView log = (ListView) findViewById(R.id.log);
        log.setAdapter(application.logger.getListAdapter());
        Button addWaypoint = (Button) findViewById(R.id.addWaypoint);

        final Handler mainHandler = new Handler();

        addWaypoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayList<StopOffPoint> list = createTestRoute();

                final ArrayList<StopOffPoint> updated1 = new ArrayList<>();
                updated1.addAll(list);
                updated1.add(0, new StopOffPoint(false, false, StopOffPoint.PointType.VIAPOINT, 1446131, 5004825, -1, 0, "", "via2 na rolich", ""));

                final ArrayList<StopOffPoint> updated2 = new ArrayList<>();
                updated2.addAll(updated1);
                updated2.add(1, new StopOffPoint(false, false, StopOffPoint.PointType.VIAPOINT, 1445848, 5004032, -1, 0, "", "via3 budejovicka", ""));

                final ArrayList<StopOffPoint> updated3 = new ArrayList<>();
                updated3.addAll(updated2);
                updated3.add(2, new StopOffPoint(false, false, StopOffPoint.PointType.VIAPOINT, 1445848, 5004132, -1, 0, "", "via4 budejovicka2", ""));

                final ArrayList<StopOffPoint> updated4 = new ArrayList<>();
                updated4.addAll(updated3);
                updated4.add(3, new StopOffPoint(false, false, StopOffPoint.PointType.VIAPOINT, 1445848, 5004232, -1, 0, "", "via5 budejovicka3", ""));

                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        application.logger.d("navman: !!! simulating incoming waypoint before navigate btn is clicked");
                        application.navigationManager.matchRoute();
                    }
                }, 2000);


                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        application.logger.d("navman: !!! simulating navigate btn clicked");
                        application.navigationManager.navigate(list, true);
                    }
                }, 3000);

                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        application.logger.d("navman: !!! simulating waypoint 1 added");
                        application.navigationManager.onActiveRouteChanged(updated1);
                    }
                }, 4000);
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        application.logger.d("navman: !!! simulating waypoint 2 added");
                        application.navigationManager.onActiveRouteChanged(updated2);
                    }
                }, 4500);
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        application.logger.d("navman: !!! simulating waypoint 3 added");
                        application.navigationManager.onActiveRouteChanged(updated3);
                    }
                }, 5000);
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        application.logger.d("navman: !!! simulating waypoint 4 added");
                        application.navigationManager.onActiveRouteChanged(updated4);
                    }
                }, 5500);
            }
        });

        Button startNavigation = (Button) findViewById(R.id.startNavigation);
        startNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                application.navigationManager.navigate(createTestRoute(), true);
            }
        });

    }

    private ArrayList<StopOffPoint> createTestRoute() {
        ArrayList<StopOffPoint> list = new ArrayList<>();
        //list.add(new StopOffPoint(false, true, StopOffPoint.PointType.START, 1446186, 5004955, -1, 0, "", "start sporilov", ""));
        list.add(new StopOffPoint(false, false, StopOffPoint.PointType.VIAPOINT, 1330165, 4928561, -1, 0, "", "via1 horakov", ""));
        list.add(new StopOffPoint(false, false, StopOffPoint.PointType.FINISH, 1225008, 4877701, -1, 0, "", "finish helmutov", ""));
        return list;
    }

}
